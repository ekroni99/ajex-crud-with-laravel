<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function product()
    {
        $product = Product::latest()->paginate(5);
        return view('product', compact('product'));
    }
    public function add_product(Request $request)
    {
        $request->validate(
            [
                'name' => 'required|unique:products',
                'price' => 'required'
            ],
            [
                'name.required' => 'name is required',
                'name.unique' => 'name already exits',
                'price.required' => 'price is required'
            ]
        );

        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->save();

        return response()->json([
            'status' => 'success',
        ]);
    }
    public function up_product(Request $request)
    {
        $request->validate(
            [
                'up_name' => 'required|unique:products,name,' . $request->up_id,
                'up_price' => 'required'
            ],
            [
                'up_name.required' => 'name is required',
                'up_name.unique' => 'name is already exits',
                'up_price.required' => 'price is required'
            ]
        );
        Product::where('id', $request->up_id)->update([
            'name' => $request->up_name,
            'price' => $request->up_price,
        ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function del_product(Request $request)
    {
        Product::find($request->del_id)->delete();

        return response()->json([
            'status' => 'success',
        ]);
    }
    public function pagination()
    {
        $product = Product::latest()->paginate(5);
        return view('product_page', compact('product'))->render();
    }

    public function search_product(Request $request)
    {
           $product=Product::where('name','like','%'.$request->search_string.'%')
           ->orWhere('price','like','%'.$request->search_string.'%')
           ->orderBy('id','desc')
           ->paginate(5);

           if($product->count() >=1){
            return view('product_page', compact('product'))->render();
           }
           else{
            return response()->json([
                'status'=>'nothing_found',
            ]);
           }

        // $product = Product::where('name', 'like', '%' . $request->search_string . '%')
        //     ->orWhere('price', '%' . $request->search_string . '%')
        //     ->orderBy('id', 'desc')
        //     ->paginate(5);
        //     if($product->count() >=1){
        //         return view('product_page',compact('product'))->render();
        //     }
        //     else{
        //         return response()->json([
        //             'status'=>'not_found',
        //         ]);
        //     }
    }
}

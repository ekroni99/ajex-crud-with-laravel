<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Ajux Crud</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h2 class="text-center my-5 fw-bold">Ajus Crud</h2>
                <a href="" class="btn btn-success my-2" data-bs-toggle="modal" data-bs-target="#addModal">Add
                    Product</a>
                <input type="text" id="search" class="form-control" name="search"
                    placeholder="filter product here..">
                <div class="table-data my-2">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product</th>
                                <th scope="col">Price</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product as $key => $products)
                                <tr>
                                    <th scope="row">{{ $key + 1 }}</th>
                                    <td>{{ $products->name }}</td>
                                    <td>{{ $products->price }}</td>
                                    <td>
                                        <a href="" class="btn btn-sm btn-success updatete_btn"
                                            data-bs-toggle="modal" data-bs-target="#updateModal"
                                            data-id="{{ $products->id }}" data-name="{{ $products->name }}"
                                            data-price="{{ $products->price }}">
                                            <i class="las la-edit"></i>
                                        </a>
                                        <a href="" class="btn btn-sm btn-danger delete_product"
                                            data-id={{ $products->id }}>
                                            <i class="las la-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {!! $product->links() !!}
                </div>
            </div>
        </div>
    </div>
    @include('product_js');
    @include('modal');
    @include('upmodal')
    {!! Toastr::message() !!}
</body>

</html>

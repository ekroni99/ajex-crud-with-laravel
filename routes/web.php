<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[ProductController::class,'product'])->name('product');
Route::post('/add_product',[ProductController::class,'add_product'])->name('add.product');
Route::post('/up_product',[ProductController::class,'up_product'])->name('up.product');
Route::post('/del_product',[ProductController::class,'del_product'])->name('del.product');
Route::get('/pagination/paginate-data',[ProductController::class,'pagination']);
Route::get('/search_product',[ProductController::class,'search_product'])->name('search.product');